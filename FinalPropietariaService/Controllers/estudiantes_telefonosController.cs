﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FinalPropietariaService.Models;

namespace FinalPropietariaService.Controllers
{
    public class estudiantes_telefonosController : Controller
    {
        private FinalDataContext db = new FinalDataContext();

        // GET: estudiantes_telefonos
        public ActionResult Index()
        {
            var estudiantes_telefonos = db.estudiantes_telefonos.Include(e => e.estudiante).Include(e => e.tipodetelefono);
            return View(estudiantes_telefonos.ToList());
        }

        // GET: estudiantes_telefonos/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            estudiantes_telefonos estudiantes_telefonos = db.estudiantes_telefonos.Find(id);
            if (estudiantes_telefonos == null)
            {
                return HttpNotFound();
            }
            return View(estudiantes_telefonos);
        }

        // GET: estudiantes_telefonos/Create
        public ActionResult Create()
        {
            ViewBag.matricula = new SelectList(db.estudiantes, "matricula", "identificacion");
            ViewBag.idtipodetelefono = new SelectList(db.tipodetelefonos, "idtipodetelefono", "tipodetelefono1");
            return View();
        }

        // POST: estudiantes_telefonos/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "idtelefono,matricula,codigodearea,codigodepais,telefono,idtipodetelefono")] estudiantes_telefonos estudiantes_telefonos)
        {
            if (ModelState.IsValid)
            {
                db.estudiantes_telefonos.Add(estudiantes_telefonos);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.matricula = new SelectList(db.estudiantes, "matricula", "identificacion", estudiantes_telefonos.matricula);
            ViewBag.idtipodetelefono = new SelectList(db.tipodetelefonos, "idtipodetelefono", "tipodetelefono1", estudiantes_telefonos.idtipodetelefono);
            return View(estudiantes_telefonos);
        }

        // GET: estudiantes_telefonos/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            estudiantes_telefonos estudiantes_telefonos = db.estudiantes_telefonos.Find(id);
            if (estudiantes_telefonos == null)
            {
                return HttpNotFound();
            }
            ViewBag.matricula = new SelectList(db.estudiantes, "matricula", "identificacion", estudiantes_telefonos.matricula);
            ViewBag.idtipodetelefono = new SelectList(db.tipodetelefonos, "idtipodetelefono", "tipodetelefono1", estudiantes_telefonos.idtipodetelefono);
            return View(estudiantes_telefonos);
        }

        // POST: estudiantes_telefonos/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "idtelefono,matricula,codigodearea,codigodepais,telefono,idtipodetelefono")] estudiantes_telefonos estudiantes_telefonos)
        {
            if (ModelState.IsValid)
            {
                db.Entry(estudiantes_telefonos).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.matricula = new SelectList(db.estudiantes, "matricula", "identificacion", estudiantes_telefonos.matricula);
            ViewBag.idtipodetelefono = new SelectList(db.tipodetelefonos, "idtipodetelefono", "tipodetelefono1", estudiantes_telefonos.idtipodetelefono);
            return View(estudiantes_telefonos);
        }

        // GET: estudiantes_telefonos/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            estudiantes_telefonos estudiantes_telefonos = db.estudiantes_telefonos.Find(id);
            if (estudiantes_telefonos == null)
            {
                return HttpNotFound();
            }
            return View(estudiantes_telefonos);
        }

        // POST: estudiantes_telefonos/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            estudiantes_telefonos estudiantes_telefonos = db.estudiantes_telefonos.Find(id);
            db.estudiantes_telefonos.Remove(estudiantes_telefonos);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
