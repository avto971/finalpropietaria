﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FinalPropietariaService.Models;

namespace FinalPropietariaService.Controllers
{
    public class medicos_tiposController : Controller
    {
        private FinalDataContext db = new FinalDataContext();

        // GET: medicos_tipos
        public ActionResult Index()
        {
            return View(db.medicos_tipos.ToList());
        }

        // GET: medicos_tipos/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            medicos_tipos medicos_tipos = db.medicos_tipos.Find(id);
            if (medicos_tipos == null)
            {
                return HttpNotFound();
            }
            return View(medicos_tipos);
        }

        // GET: medicos_tipos/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: medicos_tipos/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "idtipodemedico,tipodemedico")] medicos_tipos medicos_tipos)
        {
            if (ModelState.IsValid)
            {
                db.medicos_tipos.Add(medicos_tipos);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(medicos_tipos);
        }

        // GET: medicos_tipos/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            medicos_tipos medicos_tipos = db.medicos_tipos.Find(id);
            if (medicos_tipos == null)
            {
                return HttpNotFound();
            }
            return View(medicos_tipos);
        }

        // POST: medicos_tipos/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "idtipodemedico,tipodemedico")] medicos_tipos medicos_tipos)
        {
            if (ModelState.IsValid)
            {
                db.Entry(medicos_tipos).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(medicos_tipos);
        }

        // GET: medicos_tipos/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            medicos_tipos medicos_tipos = db.medicos_tipos.Find(id);
            if (medicos_tipos == null)
            {
                return HttpNotFound();
            }
            return View(medicos_tipos);
        }

        // POST: medicos_tipos/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            medicos_tipos medicos_tipos = db.medicos_tipos.Find(id);
            db.medicos_tipos.Remove(medicos_tipos);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
