﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FinalPropietariaService.Models;

namespace FinalPropietariaService.Controllers
{
    public class profesores_tiposController : Controller
    {
        private FinalDataContext db = new FinalDataContext();

        // GET: profesores_tipos
        public ActionResult Index()
        {
            return View(db.profesores_tipos.ToList());
        }

        // GET: profesores_tipos/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            profesores_tipos profesores_tipos = db.profesores_tipos.Find(id);
            if (profesores_tipos == null)
            {
                return HttpNotFound();
            }
            return View(profesores_tipos);
        }

        // GET: profesores_tipos/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: profesores_tipos/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "idtipodeprofesor,tipodeprofesor")] profesores_tipos profesores_tipos)
        {
            if (ModelState.IsValid)
            {
                db.profesores_tipos.Add(profesores_tipos);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(profesores_tipos);
        }

        // GET: profesores_tipos/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            profesores_tipos profesores_tipos = db.profesores_tipos.Find(id);
            if (profesores_tipos == null)
            {
                return HttpNotFound();
            }
            return View(profesores_tipos);
        }

        // POST: profesores_tipos/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "idtipodeprofesor,tipodeprofesor")] profesores_tipos profesores_tipos)
        {
            if (ModelState.IsValid)
            {
                db.Entry(profesores_tipos).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(profesores_tipos);
        }

        // GET: profesores_tipos/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            profesores_tipos profesores_tipos = db.profesores_tipos.Find(id);
            if (profesores_tipos == null)
            {
                return HttpNotFound();
            }
            return View(profesores_tipos);
        }

        // POST: profesores_tipos/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            profesores_tipos profesores_tipos = db.profesores_tipos.Find(id);
            db.profesores_tipos.Remove(profesores_tipos);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
