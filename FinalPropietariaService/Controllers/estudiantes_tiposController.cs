﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FinalPropietariaService.Models;

namespace FinalPropietariaService.Controllers
{
    public class estudiantes_tiposController : Controller
    {
        private FinalDataContext db = new FinalDataContext();

        // GET: estudiantes_tipos
        public ActionResult Index()
        {
            return View(db.estudiantes_tipos.ToList());
        }

        // GET: estudiantes_tipos/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            estudiantes_tipos estudiantes_tipos = db.estudiantes_tipos.Find(id);
            if (estudiantes_tipos == null)
            {
                return HttpNotFound();
            }
            return View(estudiantes_tipos);
        }

        // GET: estudiantes_tipos/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: estudiantes_tipos/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "idtipo,tipo")] estudiantes_tipos estudiantes_tipos)
        {
            if (ModelState.IsValid)
            {
                db.estudiantes_tipos.Add(estudiantes_tipos);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(estudiantes_tipos);
        }

        // GET: estudiantes_tipos/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            estudiantes_tipos estudiantes_tipos = db.estudiantes_tipos.Find(id);
            if (estudiantes_tipos == null)
            {
                return HttpNotFound();
            }
            return View(estudiantes_tipos);
        }

        // POST: estudiantes_tipos/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "idtipo,tipo")] estudiantes_tipos estudiantes_tipos)
        {
            if (ModelState.IsValid)
            {
                db.Entry(estudiantes_tipos).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(estudiantes_tipos);
        }

        // GET: estudiantes_tipos/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            estudiantes_tipos estudiantes_tipos = db.estudiantes_tipos.Find(id);
            if (estudiantes_tipos == null)
            {
                return HttpNotFound();
            }
            return View(estudiantes_tipos);
        }

        // POST: estudiantes_tipos/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            estudiantes_tipos estudiantes_tipos = db.estudiantes_tipos.Find(id);
            db.estudiantes_tipos.Remove(estudiantes_tipos);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
