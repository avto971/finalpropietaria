﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FinalPropietariaService.Models;

namespace FinalPropietariaService.Controllers
{
    public class ciudadesController : Controller
    {
        private FinalDataContext db = new FinalDataContext();

        // GET: ciudades
        public ActionResult Index()
        {
            return View(db.ciudades.ToList());
        }

        // GET: ciudades/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ciudade ciudade = db.ciudades.Find(id);
            if (ciudade == null)
            {
                return HttpNotFound();
            }
            return View(ciudade);
        }

        // GET: ciudades/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: ciudades/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "idciudad,ciudad")] ciudade ciudade)
        {
            if (ModelState.IsValid)
            {
                db.ciudades.Add(ciudade);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(ciudade);
        }

        // GET: ciudades/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ciudade ciudade = db.ciudades.Find(id);
            if (ciudade == null)
            {
                return HttpNotFound();
            }
            return View(ciudade);
        }

        // POST: ciudades/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "idciudad,ciudad")] ciudade ciudade)
        {
            if (ModelState.IsValid)
            {
                db.Entry(ciudade).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(ciudade);
        }

        // GET: ciudades/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ciudade ciudade = db.ciudades.Find(id);
            if (ciudade == null)
            {
                return HttpNotFound();
            }
            return View(ciudade);
        }

        // POST: ciudades/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ciudade ciudade = db.ciudades.Find(id);
            db.ciudades.Remove(ciudade);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
