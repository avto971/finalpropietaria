﻿using FinalPropietariaService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FinalPropietariaService.Controllers
{
    public class EstudiantesDireccionesTelefonosController : Controller
    {
        private FinalDataContext db = new FinalDataContext();

        // GET: EstudiantesDireccionesTelefonos
        public ActionResult Index()
        {
            return View(db.estudiantes.ToList());
        }

        // GET: EstudiantesDireccionesTelefonos/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: EstudiantesDireccionesTelefonos/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: EstudiantesDireccionesTelefonos/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: EstudiantesDireccionesTelefonos/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: EstudiantesDireccionesTelefonos/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: EstudiantesDireccionesTelefonos/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: EstudiantesDireccionesTelefonos/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
