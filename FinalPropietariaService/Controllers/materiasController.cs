﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FinalPropietariaService.Models;

namespace FinalPropietariaService.Controllers
{
    public class materiasController : Controller
    {
        private FinalDataContext db = new FinalDataContext();

        // GET: materias
        public ActionResult Index()
        {
            var materias = db.materias.Include(m => m.carrera).Include(m => m.estado).Include(m => m.plandeestudio);
            return View(materias.ToList());
        }

        // GET: materias/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            materia materia = db.materias.Find(id);
            if (materia == null)
            {
                return HttpNotFound();
            }
            return View(materia);
        }

        // GET: materias/Create
        public ActionResult Create()
        {
            ViewBag.idcarrera = new SelectList(db.carreras, "idcarrera", "carrera1");
            ViewBag.idestado = new SelectList(db.estados, "idestado", "estado1");
            ViewBag.idplan = new SelectList(db.plandeestudios, "idplan", "idplan");
            return View();
        }

        // POST: materias/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "idmateria,materia1,idcarrera,idplan,revisada,idestado")] materia materia)
        {
            if (ModelState.IsValid)
            {
                db.materias.Add(materia);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.idcarrera = new SelectList(db.carreras, "idcarrera", "carrera1", materia.idcarrera);
            ViewBag.idestado = new SelectList(db.estados, "idestado", "estado1", materia.idestado);
            ViewBag.idplan = new SelectList(db.plandeestudios, "idplan", "idplan", materia.idplan);
            return View(materia);
        }

        // GET: materias/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            materia materia = db.materias.Find(id);
            if (materia == null)
            {
                return HttpNotFound();
            }
            ViewBag.idcarrera = new SelectList(db.carreras, "idcarrera", "carrera1", materia.idcarrera);
            ViewBag.idestado = new SelectList(db.estados, "idestado", "estado1", materia.idestado);
            ViewBag.idplan = new SelectList(db.plandeestudios, "idplan", "idplan", materia.idplan);
            return View(materia);
        }

        // POST: materias/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "idmateria,materia1,idcarrera,idplan,revisada,idestado")] materia materia)
        {
            if (ModelState.IsValid)
            {
                db.Entry(materia).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.idcarrera = new SelectList(db.carreras, "idcarrera", "carrera1", materia.idcarrera);
            ViewBag.idestado = new SelectList(db.estados, "idestado", "estado1", materia.idestado);
            ViewBag.idplan = new SelectList(db.plandeestudios, "idplan", "idplan", materia.idplan);
            return View(materia);
        }

        // GET: materias/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            materia materia = db.materias.Find(id);
            if (materia == null)
            {
                return HttpNotFound();
            }
            return View(materia);
        }

        // POST: materias/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            materia materia = db.materias.Find(id);
            db.materias.Remove(materia);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
