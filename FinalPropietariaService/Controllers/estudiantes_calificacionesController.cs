﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FinalPropietariaService.Models;

namespace FinalPropietariaService.Controllers
{
    public class estudiantes_calificacionesController : Controller
    {
        private FinalDataContext db = new FinalDataContext();

        // GET: estudiantes_calificaciones
        public ActionResult Index()
        {
            var estudiantes_calificaciones = db.estudiantes_calificaciones.Include(e => e.carrera).Include(e => e.estado).Include(e => e.estudiante).Include(e => e.materia).Include(e => e.plandeestudio).Include(e => e.profesore);
            return View(estudiantes_calificaciones.ToList());
        }

        // GET: estudiantes_calificaciones/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            estudiantes_calificaciones estudiantes_calificaciones = db.estudiantes_calificaciones.Find(id);
            if (estudiantes_calificaciones == null)
            {
                return HttpNotFound();
            }
            return View(estudiantes_calificaciones);
        }

        // GET: estudiantes_calificaciones/Create
        public ActionResult Create()
        {
            ViewBag.idcarrera = new SelectList(db.carreras, "idcarrera", "carrera1");
            ViewBag.idestado = new SelectList(db.estados, "idestado", "estado1");
            ViewBag.matricula = new SelectList(db.estudiantes, "matricula", "identificacion");
            ViewBag.idmateria = new SelectList(db.materias, "idmateria", "materia1");
            ViewBag.idplan = new SelectList(db.plandeestudios, "idplan", "idplan");
            ViewBag.idprofesor = new SelectList(db.profesores, "idprofesor", "identificacion");
            return View();
        }

        // POST: estudiantes_calificaciones/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "idregistro,matricula,idcarrera,idplan,idmateria,grupo,cursada,notaparcial_1,notaparcial_2,notafinal,idprofesor,idestado")] estudiantes_calificaciones estudiantes_calificaciones)
        {
            if (ModelState.IsValid)
            {
                db.estudiantes_calificaciones.Add(estudiantes_calificaciones);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.idcarrera = new SelectList(db.carreras, "idcarrera", "carrera1", estudiantes_calificaciones.idcarrera);
            ViewBag.idestado = new SelectList(db.estados, "idestado", "estado1", estudiantes_calificaciones.idestado);
            ViewBag.matricula = new SelectList(db.estudiantes, "matricula", "identificacion", estudiantes_calificaciones.matricula);
            ViewBag.idmateria = new SelectList(db.materias, "idmateria", "materia1", estudiantes_calificaciones.idmateria);
            ViewBag.idplan = new SelectList(db.plandeestudios, "idplan", "idplan", estudiantes_calificaciones.idplan);
            ViewBag.idprofesor = new SelectList(db.profesores, "idprofesor", "identificacion", estudiantes_calificaciones.idprofesor);
            return View(estudiantes_calificaciones);
        }

        // GET: estudiantes_calificaciones/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            estudiantes_calificaciones estudiantes_calificaciones = db.estudiantes_calificaciones.Find(id);
            if (estudiantes_calificaciones == null)
            {
                return HttpNotFound();
            }
            ViewBag.idcarrera = new SelectList(db.carreras, "idcarrera", "carrera1", estudiantes_calificaciones.idcarrera);
            ViewBag.idestado = new SelectList(db.estados, "idestado", "estado1", estudiantes_calificaciones.idestado);
            ViewBag.matricula = new SelectList(db.estudiantes, "matricula", "identificacion", estudiantes_calificaciones.matricula);
            ViewBag.idmateria = new SelectList(db.materias, "idmateria", "materia1", estudiantes_calificaciones.idmateria);
            ViewBag.idplan = new SelectList(db.plandeestudios, "idplan", "idplan", estudiantes_calificaciones.idplan);
            ViewBag.idprofesor = new SelectList(db.profesores, "idprofesor", "identificacion", estudiantes_calificaciones.idprofesor);
            return View(estudiantes_calificaciones);
        }

        // POST: estudiantes_calificaciones/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "idregistro,matricula,idcarrera,idplan,idmateria,grupo,cursada,notaparcial_1,notaparcial_2,notafinal,idprofesor,idestado")] estudiantes_calificaciones estudiantes_calificaciones)
        {
            if (ModelState.IsValid)
            {
                db.Entry(estudiantes_calificaciones).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.idcarrera = new SelectList(db.carreras, "idcarrera", "carrera1", estudiantes_calificaciones.idcarrera);
            ViewBag.idestado = new SelectList(db.estados, "idestado", "estado1", estudiantes_calificaciones.idestado);
            ViewBag.matricula = new SelectList(db.estudiantes, "matricula", "identificacion", estudiantes_calificaciones.matricula);
            ViewBag.idmateria = new SelectList(db.materias, "idmateria", "materia1", estudiantes_calificaciones.idmateria);
            ViewBag.idplan = new SelectList(db.plandeestudios, "idplan", "idplan", estudiantes_calificaciones.idplan);
            ViewBag.idprofesor = new SelectList(db.profesores, "idprofesor", "identificacion", estudiantes_calificaciones.idprofesor);
            return View(estudiantes_calificaciones);
        }

        // GET: estudiantes_calificaciones/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            estudiantes_calificaciones estudiantes_calificaciones = db.estudiantes_calificaciones.Find(id);
            if (estudiantes_calificaciones == null)
            {
                return HttpNotFound();
            }
            return View(estudiantes_calificaciones);
        }

        // POST: estudiantes_calificaciones/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            estudiantes_calificaciones estudiantes_calificaciones = db.estudiantes_calificaciones.Find(id);
            db.estudiantes_calificaciones.Remove(estudiantes_calificaciones);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
