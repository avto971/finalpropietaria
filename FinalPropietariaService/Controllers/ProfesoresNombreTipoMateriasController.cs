﻿using FinalPropietariaService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FinalPropietariaService.Controllers
{
    public class ProfesoresNombreTipoMateriasController : Controller
    {
        private FinalDataContext db = new FinalDataContext();

        // GET: ProfesoresNombreTipoMaterias
        public ActionResult Index()
        {
            return View(db.profesores.ToList());
        }

        // GET: ProfesoresNombreTipoMaterias/Details/5
        public ActionResult Details(string id)
        {

            if (id == null)
                RedirectToAction("Index");

            var profrsores = new List<profesore>();
            var ctx = db.profesores;
            var matriculas = id.Split(',').ToList();

            foreach (var item in matriculas)
            {
                var profe = ctx.FirstOrDefault(x => x.identificacion == item);

                if (profe != null)
                    profrsores.Add(profe);
            }

            return View(profrsores);
        }

        // GET: ProfesoresNombreTipoMaterias/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: ProfesoresNombreTipoMaterias/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: ProfesoresNombreTipoMaterias/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: ProfesoresNombreTipoMaterias/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: ProfesoresNombreTipoMaterias/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: ProfesoresNombreTipoMaterias/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
