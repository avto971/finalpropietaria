﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FinalPropietariaService.Models;

namespace FinalPropietariaService.Controllers
{
    public class estudiantesController : Controller
    {
        private FinalDataContext db = new FinalDataContext();

        // GET: estudiantes
        public ActionResult Index()
        {
            var estudiantes = db.estudiantes.Include(e => e.carrera).Include(e => e.condicione).Include(e => e.estudiantes_tipos).Include(e => e.plandeestudio);
            return View(estudiantes.ToList());
        }

        // GET: estudiantes/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            estudiante estudiante = db.estudiantes.Find(id);
            if (estudiante == null)
            {
                return HttpNotFound();
            }
            return View(estudiante);
        }

        // GET: estudiantes/Create
        public ActionResult Create()
        {
            ViewBag.idcarrera = new SelectList(db.carreras, "idcarrera", "carrera1");
            ViewBag.idcondicion = new SelectList(db.condiciones, "idcondicion", "condicion");
            ViewBag.idtipo = new SelectList(db.estudiantes_tipos, "idtipo", "tipo");
            ViewBag.idplan = new SelectList(db.plandeestudios, "idplan", "idplan");
            return View();
        }

        // POST: estudiantes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "matricula,identificacion,nombres,apellidos,nacimiento,ingreso,trabaja,idcarrera,idplan,idtipo,indice,idcondicion")] estudiante estudiante)
        {
            if (ModelState.IsValid)
            {
                db.estudiantes.Add(estudiante);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.idcarrera = new SelectList(db.carreras, "idcarrera", "carrera1", estudiante.idcarrera);
            ViewBag.idcondicion = new SelectList(db.condiciones, "idcondicion", "condicion", estudiante.idcondicion);
            ViewBag.idtipo = new SelectList(db.estudiantes_tipos, "idtipo", "tipo", estudiante.idtipo);
            ViewBag.idplan = new SelectList(db.plandeestudios, "idplan", "idplan", estudiante.idplan);
            return View(estudiante);
        }

        // GET: estudiantes/Edit/5
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            estudiante estudiante = db.estudiantes.Find(id);
            if (estudiante == null)
            {
                return HttpNotFound();
            }
            ViewBag.idcarrera = new SelectList(db.carreras, "idcarrera", "carrera1", estudiante.idcarrera);
            ViewBag.idcondicion = new SelectList(db.condiciones, "idcondicion", "condicion", estudiante.idcondicion);
            ViewBag.idtipo = new SelectList(db.estudiantes_tipos, "idtipo", "tipo", estudiante.idtipo);
            ViewBag.idplan = new SelectList(db.plandeestudios, "idplan", "idplan", estudiante.idplan);
            return View(estudiante);
        }

        // POST: estudiantes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "matricula,identificacion,nombres,apellidos,nacimiento,ingreso,trabaja,idcarrera,idplan,idtipo,indice,idcondicion")] estudiante estudiante)
        {
            if (ModelState.IsValid)
            {
                db.Entry(estudiante).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.idcarrera = new SelectList(db.carreras, "idcarrera", "carrera1", estudiante.idcarrera);
            ViewBag.idcondicion = new SelectList(db.condiciones, "idcondicion", "condicion", estudiante.idcondicion);
            ViewBag.idtipo = new SelectList(db.estudiantes_tipos, "idtipo", "tipo", estudiante.idtipo);
            ViewBag.idplan = new SelectList(db.plandeestudios, "idplan", "idplan", estudiante.idplan);
            return View(estudiante);
        }

        // GET: estudiantes/Delete/5
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            estudiante estudiante = db.estudiantes.Find(id);
            if (estudiante == null)
            {
                return HttpNotFound();
            }
            return View(estudiante);
        }

        // POST: estudiantes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            estudiante estudiante = db.estudiantes.Find(id);
            db.estudiantes.Remove(estudiante);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
