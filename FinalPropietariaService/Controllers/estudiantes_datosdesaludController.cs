﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FinalPropietariaService.Models;

namespace FinalPropietariaService.Controllers
{
    public class estudiantes_datosdesaludController : Controller
    {
        private FinalDataContext db = new FinalDataContext();

        // GET: estudiantes_datosdesalud
        public ActionResult Index()
        {
            var estudiantes_datosdesalud = db.estudiantes_datosdesalud.Include(e => e.estudiante);
            return View(estudiantes_datosdesalud.ToList());
        }

        // GET: estudiantes_datosdesalud/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            estudiantes_datosdesalud estudiantes_datosdesalud = db.estudiantes_datosdesalud.Find(id);
            if (estudiantes_datosdesalud == null)
            {
                return HttpNotFound();
            }
            return View(estudiantes_datosdesalud);
        }

        // GET: estudiantes_datosdesalud/Create
        public ActionResult Create()
        {
            ViewBag.matricula = new SelectList(db.estudiantes, "matricula", "identificacion");
            return View();
        }

        // POST: estudiantes_datosdesalud/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "iddatossalud,matricula,encasoemergencia,tiposangre,rh,alergias,padecimientos,tratamientos,nombremedico,telefonomedico,centromedico,telefonocentromedico,extension,seguromedico")] estudiantes_datosdesalud estudiantes_datosdesalud)
        {
            if (ModelState.IsValid)
            {
                db.estudiantes_datosdesalud.Add(estudiantes_datosdesalud);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.matricula = new SelectList(db.estudiantes, "matricula", "identificacion", estudiantes_datosdesalud.matricula);
            return View(estudiantes_datosdesalud);
        }

        // GET: estudiantes_datosdesalud/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            estudiantes_datosdesalud estudiantes_datosdesalud = db.estudiantes_datosdesalud.Find(id);
            if (estudiantes_datosdesalud == null)
            {
                return HttpNotFound();
            }
            ViewBag.matricula = new SelectList(db.estudiantes, "matricula", "identificacion", estudiantes_datosdesalud.matricula);
            return View(estudiantes_datosdesalud);
        }

        // POST: estudiantes_datosdesalud/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "iddatossalud,matricula,encasoemergencia,tiposangre,rh,alergias,padecimientos,tratamientos,nombremedico,telefonomedico,centromedico,telefonocentromedico,extension,seguromedico")] estudiantes_datosdesalud estudiantes_datosdesalud)
        {
            if (ModelState.IsValid)
            {
                db.Entry(estudiantes_datosdesalud).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.matricula = new SelectList(db.estudiantes, "matricula", "identificacion", estudiantes_datosdesalud.matricula);
            return View(estudiantes_datosdesalud);
        }

        // GET: estudiantes_datosdesalud/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            estudiantes_datosdesalud estudiantes_datosdesalud = db.estudiantes_datosdesalud.Find(id);
            if (estudiantes_datosdesalud == null)
            {
                return HttpNotFound();
            }
            return View(estudiantes_datosdesalud);
        }

        // POST: estudiantes_datosdesalud/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            estudiantes_datosdesalud estudiantes_datosdesalud = db.estudiantes_datosdesalud.Find(id);
            db.estudiantes_datosdesalud.Remove(estudiantes_datosdesalud);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
