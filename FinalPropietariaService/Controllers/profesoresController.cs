﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FinalPropietariaService.Models;

namespace FinalPropietariaService.Controllers
{
    public class profesoresController : Controller
    {
        private FinalDataContext db = new FinalDataContext();

        // GET: profesores
        public ActionResult Index()
        {
            var profesores = db.profesores.Include(p => p.condicione).Include(p => p.profesores_tipos);
            return View(profesores.ToList());
        }

        // GET: profesores/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            profesore profesore = db.profesores.Find(id);
            if (profesore == null)
            {
                return HttpNotFound();
            }
            return View(profesore);
        }

        // GET: profesores/Create
        public ActionResult Create()
        {
            ViewBag.idcondicion = new SelectList(db.condiciones, "idcondicion", "condicion");
            ViewBag.idtipodeprofesor = new SelectList(db.profesores_tipos, "idtipodeprofesor", "tipodeprofesor");
            return View();
        }

        // POST: profesores/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "idprofesor,identificacion,nombres,apellidos,nacimiento,ingreso,trabaja,idtipodeprofesor,idcondicion")] profesore profesore)
        {
            if (ModelState.IsValid)
            {
                db.profesores.Add(profesore);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.idcondicion = new SelectList(db.condiciones, "idcondicion", "condicion", profesore.idcondicion);
            ViewBag.idtipodeprofesor = new SelectList(db.profesores_tipos, "idtipodeprofesor", "tipodeprofesor", profesore.idtipodeprofesor);
            return View(profesore);
        }

        // GET: profesores/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            profesore profesore = db.profesores.Find(id);
            if (profesore == null)
            {
                return HttpNotFound();
            }
            ViewBag.idcondicion = new SelectList(db.condiciones, "idcondicion", "condicion", profesore.idcondicion);
            ViewBag.idtipodeprofesor = new SelectList(db.profesores_tipos, "idtipodeprofesor", "tipodeprofesor", profesore.idtipodeprofesor);
            return View(profesore);
        }

        // POST: profesores/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "idprofesor,identificacion,nombres,apellidos,nacimiento,ingreso,trabaja,idtipodeprofesor,idcondicion")] profesore profesore)
        {
            if (ModelState.IsValid)
            {
                db.Entry(profesore).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.idcondicion = new SelectList(db.condiciones, "idcondicion", "condicion", profesore.idcondicion);
            ViewBag.idtipodeprofesor = new SelectList(db.profesores_tipos, "idtipodeprofesor", "tipodeprofesor", profesore.idtipodeprofesor);
            return View(profesore);
        }

        // GET: profesores/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            profesore profesore = db.profesores.Find(id);
            if (profesore == null)
            {
                return HttpNotFound();
            }
            return View(profesore);
        }

        // POST: profesores/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            profesore profesore = db.profesores.Find(id);
            db.profesores.Remove(profesore);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
