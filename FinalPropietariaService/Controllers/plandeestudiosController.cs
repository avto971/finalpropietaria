﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FinalPropietariaService.Models;

namespace FinalPropietariaService.Controllers
{
    public class plandeestudiosController : Controller
    {
        private FinalDataContext db = new FinalDataContext();

        // GET: plandeestudios
        public ActionResult Index()
        {
            var plandeestudios = db.plandeestudios.Include(p => p.carrera);
            return View(plandeestudios.ToList());
        }

        // GET: plandeestudios/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            plandeestudio plandeestudio = db.plandeestudios.Find(id);
            if (plandeestudio == null)
            {
                return HttpNotFound();
            }
            return View(plandeestudio);
        }

        // GET: plandeestudios/Create
        public ActionResult Create()
        {
            ViewBag.idcarrera = new SelectList(db.carreras, "idcarrera", "carrera1");
            return View();
        }

        // POST: plandeestudios/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "idplan,idcarrera,fecha,idestado")] plandeestudio plandeestudio)
        {
            if (ModelState.IsValid)
            {
                db.plandeestudios.Add(plandeestudio);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.idcarrera = new SelectList(db.carreras, "idcarrera", "carrera1", plandeestudio.idcarrera);
            return View(plandeestudio);
        }

        // GET: plandeestudios/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            plandeestudio plandeestudio = db.plandeestudios.Find(id);
            if (plandeestudio == null)
            {
                return HttpNotFound();
            }
            ViewBag.idcarrera = new SelectList(db.carreras, "idcarrera", "carrera1", plandeestudio.idcarrera);
            return View(plandeestudio);
        }

        // POST: plandeestudios/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "idplan,idcarrera,fecha,idestado")] plandeestudio plandeestudio)
        {
            if (ModelState.IsValid)
            {
                db.Entry(plandeestudio).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.idcarrera = new SelectList(db.carreras, "idcarrera", "carrera1", plandeestudio.idcarrera);
            return View(plandeestudio);
        }

        // GET: plandeestudios/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            plandeestudio plandeestudio = db.plandeestudios.Find(id);
            if (plandeestudio == null)
            {
                return HttpNotFound();
            }
            return View(plandeestudio);
        }

        // POST: plandeestudios/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            plandeestudio plandeestudio = db.plandeestudios.Find(id);
            db.plandeestudios.Remove(plandeestudio);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
