﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FinalPropietariaService.Models;

namespace FinalPropietariaService.Controllers
{
    public class estudiantes_direccionesController : Controller
    {
        private FinalDataContext db = new FinalDataContext();

        // GET: estudiantes_direcciones
        public ActionResult Index()
        {
            var estudiantes_direcciones = db.estudiantes_direcciones.Include(e => e.ciudade).Include(e => e.estudiante).Include(e => e.pais).Include(e => e.sectore).Include(e => e.tipodedireccion);
            return View(estudiantes_direcciones.ToList());
        }

        // GET: estudiantes_direcciones/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            estudiantes_direcciones estudiantes_direcciones = db.estudiantes_direcciones.Find(id);
            if (estudiantes_direcciones == null)
            {
                return HttpNotFound();
            }
            return View(estudiantes_direcciones);
        }

        // GET: estudiantes_direcciones/Create
        public ActionResult Create()
        {
            ViewBag.idciudad = new SelectList(db.ciudades, "idciudad", "ciudad");
            ViewBag.matricula = new SelectList(db.estudiantes, "matricula", "identificacion");
            ViewBag.idpais = new SelectList(db.paises, "idpais", "pais1");
            ViewBag.idsector = new SelectList(db.sectores, "idsector", "sector");
            ViewBag.idtipodedireccion = new SelectList(db.tipodedireccions, "idtipodedireccion", "tipodedireccion1");
            return View();
        }

        // POST: estudiantes_direcciones/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "iddireccion,matricula,idtipodedireccion,calle,numero,idsector,idciudad,idpais,codigopostal")] estudiantes_direcciones estudiantes_direcciones)
        {
            if (ModelState.IsValid)
            {
                db.estudiantes_direcciones.Add(estudiantes_direcciones);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.idciudad = new SelectList(db.ciudades, "idciudad", "ciudad", estudiantes_direcciones.idciudad);
            ViewBag.matricula = new SelectList(db.estudiantes, "matricula", "identificacion", estudiantes_direcciones.matricula);
            ViewBag.idpais = new SelectList(db.paises, "idpais", "pais1", estudiantes_direcciones.idpais);
            ViewBag.idsector = new SelectList(db.sectores, "idsector", "sector", estudiantes_direcciones.idsector);
            ViewBag.idtipodedireccion = new SelectList(db.tipodedireccions, "idtipodedireccion", "tipodedireccion1", estudiantes_direcciones.idtipodedireccion);
            return View(estudiantes_direcciones);
        }

        // GET: estudiantes_direcciones/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            estudiantes_direcciones estudiantes_direcciones = db.estudiantes_direcciones.Find(id);
            if (estudiantes_direcciones == null)
            {
                return HttpNotFound();
            }
            ViewBag.idciudad = new SelectList(db.ciudades, "idciudad", "ciudad", estudiantes_direcciones.idciudad);
            ViewBag.matricula = new SelectList(db.estudiantes, "matricula", "identificacion", estudiantes_direcciones.matricula);
            ViewBag.idpais = new SelectList(db.paises, "idpais", "pais1", estudiantes_direcciones.idpais);
            ViewBag.idsector = new SelectList(db.sectores, "idsector", "sector", estudiantes_direcciones.idsector);
            ViewBag.idtipodedireccion = new SelectList(db.tipodedireccions, "idtipodedireccion", "tipodedireccion1", estudiantes_direcciones.idtipodedireccion);
            return View(estudiantes_direcciones);
        }

        // POST: estudiantes_direcciones/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "iddireccion,matricula,idtipodedireccion,calle,numero,idsector,idciudad,idpais,codigopostal")] estudiantes_direcciones estudiantes_direcciones)
        {
            if (ModelState.IsValid)
            {
                db.Entry(estudiantes_direcciones).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.idciudad = new SelectList(db.ciudades, "idciudad", "ciudad", estudiantes_direcciones.idciudad);
            ViewBag.matricula = new SelectList(db.estudiantes, "matricula", "identificacion", estudiantes_direcciones.matricula);
            ViewBag.idpais = new SelectList(db.paises, "idpais", "pais1", estudiantes_direcciones.idpais);
            ViewBag.idsector = new SelectList(db.sectores, "idsector", "sector", estudiantes_direcciones.idsector);
            ViewBag.idtipodedireccion = new SelectList(db.tipodedireccions, "idtipodedireccion", "tipodedireccion1", estudiantes_direcciones.idtipodedireccion);
            return View(estudiantes_direcciones);
        }

        // GET: estudiantes_direcciones/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            estudiantes_direcciones estudiantes_direcciones = db.estudiantes_direcciones.Find(id);
            if (estudiantes_direcciones == null)
            {
                return HttpNotFound();
            }
            return View(estudiantes_direcciones);
        }

        // POST: estudiantes_direcciones/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            estudiantes_direcciones estudiantes_direcciones = db.estudiantes_direcciones.Find(id);
            db.estudiantes_direcciones.Remove(estudiantes_direcciones);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
