﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FinalPropietariaService.Models;

namespace FinalPropietariaService.Controllers
{
    public class sectoresController : Controller
    {
        private FinalDataContext db = new FinalDataContext();

        // GET: sectores
        public ActionResult Index()
        {
            return View(db.sectores.ToList());
        }

        // GET: sectores/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            sectore sectore = db.sectores.Find(id);
            if (sectore == null)
            {
                return HttpNotFound();
            }
            return View(sectore);
        }

        // GET: sectores/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: sectores/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "idsector,sector")] sectore sectore)
        {
            if (ModelState.IsValid)
            {
                db.sectores.Add(sectore);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(sectore);
        }

        // GET: sectores/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            sectore sectore = db.sectores.Find(id);
            if (sectore == null)
            {
                return HttpNotFound();
            }
            return View(sectore);
        }

        // POST: sectores/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "idsector,sector")] sectore sectore)
        {
            if (ModelState.IsValid)
            {
                db.Entry(sectore).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(sectore);
        }

        // GET: sectores/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            sectore sectore = db.sectores.Find(id);
            if (sectore == null)
            {
                return HttpNotFound();
            }
            return View(sectore);
        }

        // POST: sectores/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            sectore sectore = db.sectores.Find(id);
            db.sectores.Remove(sectore);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
