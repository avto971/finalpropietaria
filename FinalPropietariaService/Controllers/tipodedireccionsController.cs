﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FinalPropietariaService.Models;

namespace FinalPropietariaService.Controllers
{
    public class tipodedireccionsController : Controller
    {
        private FinalDataContext db = new FinalDataContext();

        // GET: tipodedireccions
        public ActionResult Index()
        {
            return View(db.tipodedireccions.ToList());
        }

        // GET: tipodedireccions/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tipodedireccion tipodedireccion = db.tipodedireccions.Find(id);
            if (tipodedireccion == null)
            {
                return HttpNotFound();
            }
            return View(tipodedireccion);
        }

        // GET: tipodedireccions/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: tipodedireccions/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "idtipodedireccion,tipodedireccion1")] tipodedireccion tipodedireccion)
        {
            if (ModelState.IsValid)
            {
                db.tipodedireccions.Add(tipodedireccion);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(tipodedireccion);
        }

        // GET: tipodedireccions/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tipodedireccion tipodedireccion = db.tipodedireccions.Find(id);
            if (tipodedireccion == null)
            {
                return HttpNotFound();
            }
            return View(tipodedireccion);
        }

        // POST: tipodedireccions/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "idtipodedireccion,tipodedireccion1")] tipodedireccion tipodedireccion)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tipodedireccion).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(tipodedireccion);
        }

        // GET: tipodedireccions/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tipodedireccion tipodedireccion = db.tipodedireccions.Find(id);
            if (tipodedireccion == null)
            {
                return HttpNotFound();
            }
            return View(tipodedireccion);
        }

        // POST: tipodedireccions/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            tipodedireccion tipodedireccion = db.tipodedireccions.Find(id);
            db.tipodedireccions.Remove(tipodedireccion);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
