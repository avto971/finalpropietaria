﻿using FinalPropietariaService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FinalPropietariaService.Controllers
{
    public class EstudianteCarreaPlanController : Controller
    {
        private FinalDataContext db = new FinalDataContext();

        // GET: EstudianteCarreaPlan
        public ActionResult Index()
        {
            return View(db.estudiantes.ToList());
        }

        // GET: EstudianteCarreaPlan/Details/5
        public ActionResult Details(string id)
        {

            if (id == null)
                RedirectToAction("Index");

            var estudiantes = new List<estudiante>();
            var ctx = db.estudiantes;
            var matriculas = id.Split(',').ToList();

            foreach (var item in matriculas)
            {
               var estu  = ctx.FirstOrDefault(x => x.matricula == item);

                if (estu != null)
                    estudiantes.Add(estu);
            }

            return View(estudiantes);
        }

        // GET: EstudianteCarreaPlan/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: EstudianteCarreaPlan/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: EstudianteCarreaPlan/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: EstudianteCarreaPlan/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: EstudianteCarreaPlan/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: EstudianteCarreaPlan/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
