﻿using FinalPropietariaService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FinalPropietariaService.Controllers
{
    public class EstudianteMatriculaCalificacionesController : Controller
    {
        private FinalDataContext db = new FinalDataContext();

        // GET: EstudianteMatriculaCalificaciones
        public ActionResult Index()
        {
            return View(db.estudiantes.ToList());
        }

        // GET: EstudianteMatriculaCalificaciones/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: EstudianteMatriculaCalificaciones/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: EstudianteMatriculaCalificaciones/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: EstudianteMatriculaCalificaciones/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: EstudianteMatriculaCalificaciones/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: EstudianteMatriculaCalificaciones/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: EstudianteMatriculaCalificaciones/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
