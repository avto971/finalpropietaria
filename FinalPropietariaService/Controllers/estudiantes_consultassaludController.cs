﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FinalPropietariaService.Models;

namespace FinalPropietariaService.Controllers
{
    public class estudiantes_consultassaludController : Controller
    {
        private FinalDataContext db = new FinalDataContext();

        // GET: estudiantes_consultassalud
        public ActionResult Index()
        {
            var estudiantes_consultassalud = db.estudiantes_consultassalud.Include(e => e.estudiante).Include(e => e.medico);
            return View(estudiantes_consultassalud.ToList());
        }

        // GET: estudiantes_consultassalud/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            estudiantes_consultassalud estudiantes_consultassalud = db.estudiantes_consultassalud.Find(id);
            if (estudiantes_consultassalud == null)
            {
                return HttpNotFound();
            }
            return View(estudiantes_consultassalud);
        }

        // GET: estudiantes_consultassalud/Create
        public ActionResult Create()
        {
            ViewBag.matricula = new SelectList(db.estudiantes, "matricula", "identificacion");
            ViewBag.idmedico = new SelectList(db.medicos, "idmedico", "identificacion");
            return View();
        }

        // POST: estudiantes_consultassalud/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "idsalud,matricula,idmedico,iddiagnostico,diagnostico,fecha")] estudiantes_consultassalud estudiantes_consultassalud)
        {
            if (ModelState.IsValid)
            {
                db.estudiantes_consultassalud.Add(estudiantes_consultassalud);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.matricula = new SelectList(db.estudiantes, "matricula", "identificacion", estudiantes_consultassalud.matricula);
            ViewBag.idmedico = new SelectList(db.medicos, "idmedico", "identificacion", estudiantes_consultassalud.idmedico);
            return View(estudiantes_consultassalud);
        }

        // GET: estudiantes_consultassalud/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            estudiantes_consultassalud estudiantes_consultassalud = db.estudiantes_consultassalud.Find(id);
            if (estudiantes_consultassalud == null)
            {
                return HttpNotFound();
            }
            ViewBag.matricula = new SelectList(db.estudiantes, "matricula", "identificacion", estudiantes_consultassalud.matricula);
            ViewBag.idmedico = new SelectList(db.medicos, "idmedico", "identificacion", estudiantes_consultassalud.idmedico);
            return View(estudiantes_consultassalud);
        }

        // POST: estudiantes_consultassalud/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "idsalud,matricula,idmedico,iddiagnostico,diagnostico,fecha")] estudiantes_consultassalud estudiantes_consultassalud)
        {
            if (ModelState.IsValid)
            {
                db.Entry(estudiantes_consultassalud).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.matricula = new SelectList(db.estudiantes, "matricula", "identificacion", estudiantes_consultassalud.matricula);
            ViewBag.idmedico = new SelectList(db.medicos, "idmedico", "identificacion", estudiantes_consultassalud.idmedico);
            return View(estudiantes_consultassalud);
        }

        // GET: estudiantes_consultassalud/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            estudiantes_consultassalud estudiantes_consultassalud = db.estudiantes_consultassalud.Find(id);
            if (estudiantes_consultassalud == null)
            {
                return HttpNotFound();
            }
            return View(estudiantes_consultassalud);
        }

        // POST: estudiantes_consultassalud/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            estudiantes_consultassalud estudiantes_consultassalud = db.estudiantes_consultassalud.Find(id);
            db.estudiantes_consultassalud.Remove(estudiantes_consultassalud);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
