﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FinalPropietariaService.Models;

namespace FinalPropietariaService.Controllers
{
    public class medicosController : Controller
    {
        private FinalDataContext db = new FinalDataContext();

        // GET: medicos
        public ActionResult Index()
        {
            var medicos = db.medicos.Include(m => m.medicos_tipos);
            return View(medicos.ToList());
        }

        // GET: medicos/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            medico medico = db.medicos.Find(id);
            if (medico == null)
            {
                return HttpNotFound();
            }
            return View(medico);
        }

        // GET: medicos/Create
        public ActionResult Create()
        {
            ViewBag.idtipodemedico = new SelectList(db.medicos_tipos, "idtipodemedico", "tipodemedico");
            return View();
        }

        // POST: medicos/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "idmedico,identificacion,nombres,apellidos,nacimiento,ingreso,trabaja,idtipodemedico,idcondicion")] medico medico)
        {
            if (ModelState.IsValid)
            {
                db.medicos.Add(medico);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.idtipodemedico = new SelectList(db.medicos_tipos, "idtipodemedico", "tipodemedico", medico.idtipodemedico);
            return View(medico);
        }

        // GET: medicos/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            medico medico = db.medicos.Find(id);
            if (medico == null)
            {
                return HttpNotFound();
            }
            ViewBag.idtipodemedico = new SelectList(db.medicos_tipos, "idtipodemedico", "tipodemedico", medico.idtipodemedico);
            return View(medico);
        }

        // POST: medicos/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "idmedico,identificacion,nombres,apellidos,nacimiento,ingreso,trabaja,idtipodemedico,idcondicion")] medico medico)
        {
            if (ModelState.IsValid)
            {
                db.Entry(medico).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.idtipodemedico = new SelectList(db.medicos_tipos, "idtipodemedico", "tipodemedico", medico.idtipodemedico);
            return View(medico);
        }

        // GET: medicos/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            medico medico = db.medicos.Find(id);
            if (medico == null)
            {
                return HttpNotFound();
            }
            return View(medico);
        }

        // POST: medicos/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            medico medico = db.medicos.Find(id);
            db.medicos.Remove(medico);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
