﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FinalPropietariaService.Models;

namespace FinalPropietariaService.Controllers
{
    public class tipodetelefonoesController : Controller
    {
        private FinalDataContext db = new FinalDataContext();

        // GET: tipodetelefonoes
        public ActionResult Index()
        {
            return View(db.tipodetelefonos.ToList());
        }

        // GET: tipodetelefonoes/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tipodetelefono tipodetelefono = db.tipodetelefonos.Find(id);
            if (tipodetelefono == null)
            {
                return HttpNotFound();
            }
            return View(tipodetelefono);
        }

        // GET: tipodetelefonoes/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: tipodetelefonoes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "idtipodetelefono,tipodetelefono1")] tipodetelefono tipodetelefono)
        {
            if (ModelState.IsValid)
            {
                db.tipodetelefonos.Add(tipodetelefono);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(tipodetelefono);
        }

        // GET: tipodetelefonoes/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tipodetelefono tipodetelefono = db.tipodetelefonos.Find(id);
            if (tipodetelefono == null)
            {
                return HttpNotFound();
            }
            return View(tipodetelefono);
        }

        // POST: tipodetelefonoes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "idtipodetelefono,tipodetelefono1")] tipodetelefono tipodetelefono)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tipodetelefono).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(tipodetelefono);
        }

        // GET: tipodetelefonoes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tipodetelefono tipodetelefono = db.tipodetelefonos.Find(id);
            if (tipodetelefono == null)
            {
                return HttpNotFound();
            }
            return View(tipodetelefono);
        }

        // POST: tipodetelefonoes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            tipodetelefono tipodetelefono = db.tipodetelefonos.Find(id);
            db.tipodetelefonos.Remove(tipodetelefono);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
