namespace FinalPropietariaService.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class materia
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int idmateria { get; set; }

        [Column("materia")]
        [Required]
        [StringLength(50)]
        public string materia1 { get; set; }

        public int idcarrera { get; set; }

        public int idplan { get; set; }

        [Column(TypeName = "date")]
        public DateTime revisada { get; set; }

        public int idestado { get; set; }

        public virtual carrera carrera { get; set; }

        public virtual estado estado { get; set; }

        public virtual plandeestudio plandeestudio { get; set; }
    }
}
