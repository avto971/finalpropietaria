namespace FinalPropietariaService.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class estudiantes_datosdesalud
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int iddatossalud { get; set; }

        [Required]
        [StringLength(8)]
        public string matricula { get; set; }

        [Required]
        [StringLength(50)]
        public string encasoemergencia { get; set; }

        [Required]
        [StringLength(2)]
        public string tiposangre { get; set; }

        [Required]
        [StringLength(1)]
        public string rh { get; set; }

        [Required]
        [StringLength(150)]
        public string alergias { get; set; }

        [Required]
        [StringLength(150)]
        public string padecimientos { get; set; }

        [Required]
        [StringLength(150)]
        public string tratamientos { get; set; }

        [Required]
        [StringLength(50)]
        public string nombremedico { get; set; }

        [Required]
        [StringLength(10)]
        public string telefonomedico { get; set; }

        [Required]
        [StringLength(150)]
        public string centromedico { get; set; }

        [Required]
        [StringLength(10)]
        public string telefonocentromedico { get; set; }

        [Required]
        [StringLength(5)]
        public string extension { get; set; }

        [Required]
        [StringLength(30)]
        public string seguromedico { get; set; }

        public virtual estudiante estudiante { get; set; }
    }
}
