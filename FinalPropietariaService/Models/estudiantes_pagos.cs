namespace FinalPropietariaService.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class estudiantes_pagos
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int idpago { get; set; }

        [Required]
        [StringLength(8)]
        public string matricula { get; set; }

        [Column(TypeName = "date")]
        public DateTime fecha { get; set; }

        public double monto { get; set; }

        public int idtipodepago { get; set; }

        public int idmediodepago { get; set; }

        public int numerodeautorizacion { get; set; }

        public int cuentacontable { get; set; }

        public virtual estudiante estudiante { get; set; }

        public virtual mediosdepago mediosdepago { get; set; }

        public virtual tipodepago tipodepago { get; set; }
    }
}
