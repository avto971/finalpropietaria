namespace FinalPropietariaService.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class estudiantes_direcciones
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int iddireccion { get; set; }

        [Required]
        [StringLength(8)]
        public string matricula { get; set; }

        public int idtipodedireccion { get; set; }

        [Required]
        [StringLength(50)]
        public string calle { get; set; }

        public int numero { get; set; }

        public int idsector { get; set; }

        public int idciudad { get; set; }

        public int idpais { get; set; }

        public int codigopostal { get; set; }

        public virtual ciudade ciudade { get; set; }

        public virtual estudiante estudiante { get; set; }

        public virtual pais pais { get; set; }

        public virtual sectore sectore { get; set; }

        public virtual tipodedireccion tipodedireccion { get; set; }
    }
}
