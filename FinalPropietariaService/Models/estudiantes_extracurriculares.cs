namespace FinalPropietariaService.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class estudiantes_extracurriculares
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int idextracurricular { get; set; }

        [Required]
        [StringLength(8)]
        public string matricula { get; set; }

        public int idtipoactividad { get; set; }

        public int actividad { get; set; }

        [Column(TypeName = "date")]
        public DateTime fecha { get; set; }

        public double horas { get; set; }

        public int idestado { get; set; }

        public virtual estado estado { get; set; }

        public virtual estudiante estudiante { get; set; }

        public virtual tipodeactividade tipodeactividade { get; set; }
    }
}
