namespace FinalPropietariaService.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class estudiantes_consultassalud
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int idsalud { get; set; }

        [Required]
        [StringLength(8)]
        public string matricula { get; set; }

        public int idmedico { get; set; }

        public int iddiagnostico { get; set; }

        [Column(TypeName = "ntext")]
        [Required]
        public string diagnostico { get; set; }

        [Column(TypeName = "date")]
        public DateTime fecha { get; set; }

        public virtual estudiante estudiante { get; set; }

        public virtual medico medico { get; set; }
    }
}
