namespace FinalPropietariaService.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class estudiantes_telefonos
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int idtelefono { get; set; }

        [Required]
        [StringLength(8)]
        public string matricula { get; set; }

        public int codigodearea { get; set; }

        public int codigodepais { get; set; }

        public int telefono { get; set; }

        public int idtipodetelefono { get; set; }

        public virtual estudiante estudiante { get; set; }

        public virtual tipodetelefono tipodetelefono { get; set; }
    }
}
