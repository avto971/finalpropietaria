namespace FinalPropietariaService.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class medico
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public medico()
        {
            estudiantes_consultassalud = new HashSet<estudiantes_consultassalud>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int idmedico { get; set; }

        [Required]
        [StringLength(11)]
        public string identificacion { get; set; }

        [Required]
        [StringLength(30)]
        public string nombres { get; set; }

        [Required]
        [StringLength(30)]
        public string apellidos { get; set; }

        [Column(TypeName = "date")]
        public DateTime nacimiento { get; set; }

        [Column(TypeName = "date")]
        public DateTime ingreso { get; set; }

        public bool trabaja { get; set; }

        public int idtipodemedico { get; set; }

        public int idcondicion { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<estudiantes_consultassalud> estudiantes_consultassalud { get; set; }

        public virtual medicos_tipos medicos_tipos { get; set; }
    }
}
