namespace FinalPropietariaService.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class FinalDataContext : DbContext
    {
        public FinalDataContext()
            : base("name=FinalDataContext")
        {
        }

        public virtual DbSet<carrera> carreras { get; set; }
        public virtual DbSet<ciudade> ciudades { get; set; }
        public virtual DbSet<condicione> condiciones { get; set; }
        public virtual DbSet<estado> estados { get; set; }
        public virtual DbSet<estudiante> estudiantes { get; set; }
        public virtual DbSet<estudiantes_calificaciones> estudiantes_calificaciones { get; set; }
        public virtual DbSet<estudiantes_consultassalud> estudiantes_consultassalud { get; set; }
        public virtual DbSet<estudiantes_datosdesalud> estudiantes_datosdesalud { get; set; }
        public virtual DbSet<estudiantes_direcciones> estudiantes_direcciones { get; set; }
        public virtual DbSet<estudiantes_extracurriculares> estudiantes_extracurriculares { get; set; }
        public virtual DbSet<estudiantes_pagos> estudiantes_pagos { get; set; }
        public virtual DbSet<estudiantes_telefonos> estudiantes_telefonos { get; set; }
        public virtual DbSet<estudiantes_tipos> estudiantes_tipos { get; set; }
        public virtual DbSet<materia> materias { get; set; }
        public virtual DbSet<medico> medicos { get; set; }
        public virtual DbSet<medicos_tipos> medicos_tipos { get; set; }
        public virtual DbSet<mediosdepago> mediosdepagoes { get; set; }
        public virtual DbSet<pais> paises { get; set; }
        public virtual DbSet<plandeestudio> plandeestudios { get; set; }
        public virtual DbSet<profesore> profesores { get; set; }
        public virtual DbSet<profesores_tipos> profesores_tipos { get; set; }
        public virtual DbSet<sectore> sectores { get; set; }
        public virtual DbSet<tipodeactividade> tipodeactividades { get; set; }
        public virtual DbSet<tipodedireccion> tipodedireccions { get; set; }
        public virtual DbSet<tipodepago> tipodepagos { get; set; }
        public virtual DbSet<tipodetelefono> tipodetelefonos { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<carrera>()
                .HasMany(e => e.estudiantes_calificaciones)
                .WithRequired(e => e.carrera)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<carrera>()
                .HasMany(e => e.materias)
                .WithRequired(e => e.carrera)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<carrera>()
                .HasMany(e => e.plandeestudios)
                .WithRequired(e => e.carrera)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ciudade>()
                .Property(e => e.ciudad)
                .IsFixedLength();

            modelBuilder.Entity<ciudade>()
                .HasMany(e => e.estudiantes_direcciones)
                .WithRequired(e => e.ciudade)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<condicione>()
                .Property(e => e.condicion)
                .IsFixedLength();

            modelBuilder.Entity<condicione>()
                .HasMany(e => e.estudiantes)
                .WithRequired(e => e.condicione)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<condicione>()
                .HasMany(e => e.profesores)
                .WithRequired(e => e.condicione)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<estado>()
                .Property(e => e.estado1)
                .IsFixedLength();

            modelBuilder.Entity<estado>()
                .HasMany(e => e.estudiantes_calificaciones)
                .WithRequired(e => e.estado)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<estado>()
                .HasMany(e => e.estudiantes_extracurriculares)
                .WithRequired(e => e.estado)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<estado>()
                .HasMany(e => e.materias)
                .WithRequired(e => e.estado)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<estudiante>()
                .Property(e => e.matricula)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<estudiante>()
                .Property(e => e.identificacion)
                .IsFixedLength();

            modelBuilder.Entity<estudiante>()
                .Property(e => e.apellidos)
                .IsFixedLength();

            //modelBuilder.Entity<estudiante>()
            //    .Property(e => e.idcarrera)
            //    .IsFixedLength();

            modelBuilder.Entity<estudiante>()
                .HasMany(e => e.estudiantes_consultassalud)
                .WithRequired(e => e.estudiante)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<estudiante>()
                .HasMany(e => e.estudiantes_datosdesalud)
                .WithRequired(e => e.estudiante)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<estudiante>()
                .HasMany(e => e.estudiantes_direcciones)
                .WithRequired(e => e.estudiante)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<estudiante>()
                .HasMany(e => e.estudiantes_extracurriculares)
                .WithRequired(e => e.estudiante)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<estudiante>()
                .HasMany(e => e.estudiantes_calificaciones)
                .WithRequired(e => e.estudiante)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<estudiante>()
                .HasMany(e => e.estudiantes_pagos)
                .WithRequired(e => e.estudiante)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<estudiante>()
                .HasMany(e => e.estudiantes_telefonos)
                .WithRequired(e => e.estudiante)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<estudiantes_calificaciones>()
                .Property(e => e.matricula)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<estudiantes_calificaciones>()
                .Property(e => e.grupo)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<estudiantes_consultassalud>()
                .Property(e => e.matricula)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<estudiantes_datosdesalud>()
                .Property(e => e.matricula)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<estudiantes_datosdesalud>()
                .Property(e => e.tiposangre)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<estudiantes_datosdesalud>()
                .Property(e => e.rh)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<estudiantes_datosdesalud>()
                .Property(e => e.telefonomedico)
                .IsFixedLength();

            modelBuilder.Entity<estudiantes_datosdesalud>()
                .Property(e => e.telefonocentromedico)
                .IsFixedLength();

            modelBuilder.Entity<estudiantes_datosdesalud>()
                .Property(e => e.extension)
                .IsFixedLength();

            modelBuilder.Entity<estudiantes_datosdesalud>()
                .Property(e => e.seguromedico)
                .IsFixedLength();

            modelBuilder.Entity<estudiantes_direcciones>()
                .Property(e => e.matricula)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<estudiantes_extracurriculares>()
                .Property(e => e.matricula)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<estudiantes_pagos>()
                .Property(e => e.matricula)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<estudiantes_telefonos>()
                .Property(e => e.matricula)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<estudiantes_tipos>()
                .Property(e => e.tipo)
                .IsFixedLength();

            modelBuilder.Entity<estudiantes_tipos>()
                .HasMany(e => e.estudiantes)
                .WithRequired(e => e.estudiantes_tipos)
                .WillCascadeOnDelete(false);


            modelBuilder.Entity<medico>()
                .Property(e => e.identificacion)
                .IsFixedLength();

            modelBuilder.Entity<medico>()
                .Property(e => e.apellidos)
                .IsFixedLength();

            modelBuilder.Entity<medico>()
                .HasMany(e => e.estudiantes_consultassalud)
                .WithRequired(e => e.medico)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<medicos_tipos>()
                .Property(e => e.tipodemedico)
                .IsFixedLength();

            modelBuilder.Entity<medicos_tipos>()
                .HasMany(e => e.medicos)
                .WithRequired(e => e.medicos_tipos)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<mediosdepago>()
                .Property(e => e.mediodepago)
                .IsFixedLength();

            modelBuilder.Entity<mediosdepago>()
                .HasMany(e => e.estudiantes_pagos)
                .WithRequired(e => e.mediosdepago)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<pais>()
                .Property(e => e.pais1)
                .IsFixedLength();

            modelBuilder.Entity<pais>()
                .HasMany(e => e.estudiantes_direcciones)
                .WithRequired(e => e.pais)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<plandeestudio>()
                .HasMany(e => e.estudiantes)
                .WithRequired(e => e.plandeestudio)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<plandeestudio>()
                .HasMany(e => e.estudiantes_calificaciones)
                .WithRequired(e => e.plandeestudio)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<plandeestudio>()
                .HasMany(e => e.materias)
                .WithRequired(e => e.plandeestudio)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<profesore>()
                .Property(e => e.identificacion)
                .IsFixedLength();

            modelBuilder.Entity<profesore>()
                .Property(e => e.apellidos)
                .IsFixedLength();

            modelBuilder.Entity<profesore>()
                .HasMany(e => e.estudiantes_calificaciones)
                .WithRequired(e => e.profesore)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<profesores_tipos>()
                .Property(e => e.tipodeprofesor)
                .IsFixedLength();

            modelBuilder.Entity<profesores_tipos>()
                .HasMany(e => e.profesores)
                .WithRequired(e => e.profesores_tipos)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<sectore>()
                .Property(e => e.sector)
                .IsFixedLength();

            modelBuilder.Entity<sectore>()
                .HasMany(e => e.estudiantes_direcciones)
                .WithRequired(e => e.sectore)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<tipodeactividade>()
                .Property(e => e.descripcionactividad)
                .IsFixedLength();

            modelBuilder.Entity<tipodeactividade>()
                .HasMany(e => e.estudiantes_extracurriculares)
                .WithRequired(e => e.tipodeactividade)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<tipodedireccion>()
                .Property(e => e.tipodedireccion1)
                .IsFixedLength();

            modelBuilder.Entity<tipodedireccion>()
                .HasMany(e => e.estudiantes_direcciones)
                .WithRequired(e => e.tipodedireccion)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<tipodepago>()
                .Property(e => e.tipodepago1)
                .IsFixedLength();

            modelBuilder.Entity<tipodepago>()
                .HasMany(e => e.estudiantes_pagos)
                .WithRequired(e => e.tipodepago)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<tipodetelefono>()
                .Property(e => e.tipodetelefono1)
                .IsFixedLength();

            modelBuilder.Entity<tipodetelefono>()
                .HasMany(e => e.estudiantes_telefonos)
                .WithRequired(e => e.tipodetelefono)
                .WillCascadeOnDelete(false);
        }
    }
}
