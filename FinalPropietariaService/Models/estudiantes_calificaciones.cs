namespace FinalPropietariaService.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class estudiantes_calificaciones
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int idregistro { get; set; }

        [Required]
        [StringLength(8)]
        public string matricula { get; set; }

        public int idcarrera { get; set; }

        public int idplan { get; set; }
        [ForeignKey("materia")]
        public int idmateria { get; set; }

        [Required]
        [StringLength(5)]
        public string grupo { get; set; }

        [Column(TypeName = "date")]
        public DateTime cursada { get; set; }

        public double notaparcial_1 { get; set; }

        public double notaparcial_2 { get; set; }

        public double notafinal { get; set; }

        public int idprofesor { get; set; }

        public int idestado { get; set; }

        public virtual carrera carrera { get; set; }

        public virtual estado estado { get; set; }

        public virtual estudiante estudiante { get; set; }

        public virtual plandeestudio plandeestudio { get; set; }

        public virtual profesore profesore { get; set; }
        public virtual materia materia { get; set; }
    }
}
