namespace FinalPropietariaService.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class profesore
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public profesore()
        {
            estudiantes_calificaciones = new HashSet<estudiantes_calificaciones>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int idprofesor { get; set; }

        [Required]
        [StringLength(11)]
        public string identificacion { get; set; }

        [Required]
        [StringLength(30)]
        public string nombres { get; set; }

        [Required]
        [StringLength(30)]
        public string apellidos { get; set; }

        [Column(TypeName = "date")]
        public DateTime nacimiento { get; set; }

        [Column(TypeName = "date")]
        public DateTime ingreso { get; set; }

        public bool trabaja { get; set; }

        public int idtipodeprofesor { get; set; }

        public int idcondicion { get; set; }

        public virtual condicione condicione { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<estudiantes_calificaciones> estudiantes_calificaciones { get; set; }

        public virtual profesores_tipos profesores_tipos { get; set; }
    }
}
