namespace FinalPropietariaService.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class estudiante
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public estudiante()
        {
            estudiantes_consultassalud = new HashSet<estudiantes_consultassalud>();
            estudiantes_datosdesalud = new HashSet<estudiantes_datosdesalud>();
            estudiantes_direcciones = new HashSet<estudiantes_direcciones>();
            estudiantes_extracurriculares = new HashSet<estudiantes_extracurriculares>();
            estudiantes_calificaciones = new HashSet<estudiantes_calificaciones>();
            estudiantes_pagos = new HashSet<estudiantes_pagos>();
            estudiantes_telefonos = new HashSet<estudiantes_telefonos>();
        }

        [Key]
        [StringLength(8)]
        public string matricula { get; set; }

        [Required]
        [StringLength(11)]
        public string identificacion { get; set; }

        [Required]
        [StringLength(30)]
        public string nombres { get; set; }

        [Required]
        [StringLength(30)]
        public string apellidos { get; set; }

        [Column(TypeName = "date")]
        public DateTime nacimiento { get; set; }

        [Column(TypeName = "date")]
        public DateTime ingreso { get; set; }

        public bool trabaja { get; set; }

        [Required]
        [ForeignKey("carrera")]
        public int idcarrera { get; set; }

        public int idplan { get; set; }

        public int idtipo { get; set; }

        public double indice { get; set; }

        public int idcondicion { get; set; }

        public virtual condicione condicione { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<estudiantes_consultassalud> estudiantes_consultassalud { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<estudiantes_datosdesalud> estudiantes_datosdesalud { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<estudiantes_direcciones> estudiantes_direcciones { get; set; }

        public virtual estudiantes_tipos estudiantes_tipos { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<estudiantes_extracurriculares> estudiantes_extracurriculares { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<estudiantes_calificaciones> estudiantes_calificaciones { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<estudiantes_pagos> estudiantes_pagos { get; set; }

        public virtual plandeestudio plandeestudio { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<estudiantes_telefonos> estudiantes_telefonos { get; set; }

        public virtual carrera carrera { get; set; }

    }
}
